package com.example.pouria.myapplication;

/**
 * Created by Pouria on 5/25/2018.
 */
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;


public class NearByAdapter extends RecyclerView.Adapter<NearByAdapter.MyViewHolder> {

    private List<NearBy> moviesList;
    private Context context;
    private  View itemView;
    FirstFragment fragment;


    private static ClickListener clickListener;

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener  {
        public TextView title, type, genre;
        private Button button1, button2;




        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            genre = (TextView) view.findViewById(R.id.genre);
            button1 = (Button) view.findViewById(R.id.button_loc);
            button2 = (Button) view.findViewById(R.id.button_proc);
//            type = (TextView) view.findViewById(R.id.type);
            view.setOnClickListener(this);
//            year = (TextView) view.findViewById(R.id.year);
        }

        @Override
        public void onClick(View v) {
            clickListener.onItemClick(getAdapterPosition(), v);
        }
    }


    public NearByAdapter(List<NearBy> moviesList, FirstFragment fragment) {
        this.moviesList = moviesList;
        this.fragment = fragment;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
         itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.nearby_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final NearBy movie = moviesList.get(position);
        holder.title.setText(movie.getTitle());
        holder.title.setTypeface(G.iransens_light);
        holder.genre.setText(movie.getGenre());
        holder.genre.setTypeface(G.iransens_light);

        holder.button1.setTypeface(G.iransens_light);
        holder.button2.setTypeface(G.iransens_light);

        holder.button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment.requestDirection(movie.getYear());
                fragment.hideRecycelerView();
//                fragment.stopRipple();
            }
        });
//        holder.type.setText(movie.getYear());
//        holder.type.setTypeface(G.iransens_light);

        holder.button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(fragment.getActivity(), ContentActivity.class);
                myIntent.putExtra("Address", "Rasht"); //Optional parameters
                fragment.startActivity(myIntent);
            }
        });


    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
    public void setOnItemClickListener(ClickListener clickListener) {
        NearByAdapter.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(int position, View v);
    }
}

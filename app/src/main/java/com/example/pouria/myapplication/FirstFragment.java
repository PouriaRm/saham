package com.example.pouria.myapplication;

/**
 * Created by Pouria on 5/18/2018.
 */

import android.annotation.SuppressLint;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


import com.akexorcist.googledirection.DirectionCallback;
import com.akexorcist.googledirection.GoogleDirection;
import com.akexorcist.googledirection.constant.TransportMode;
import com.akexorcist.googledirection.model.Direction;
import com.akexorcist.googledirection.model.Route;
import com.akexorcist.googledirection.util.DirectionConverter;
import com.arsy.maps_library.MapRipple;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Dash;
import com.google.android.gms.maps.model.Gap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PatternItem;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.maps.android.data.Feature;
import com.google.maps.android.data.kml.KmlContainer;
import com.google.maps.android.data.kml.KmlLayer;
import com.google.maps.android.data.kml.KmlPlacemark;
import com.google.maps.android.data.kml.KmlPolygon;

import org.xmlpull.v1.XmlPullParserException;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import static com.example.pouria.myapplication.G.context;


public class FirstFragment extends Fragment implements OnMapReadyCallback ,GoogleMap.OnMarkerClickListener {
    private static final String TAG = FirstFragment.class.getSimpleName();
    private static final int REQ_PERMISSION = 0x8000000;

    private GoogleMap mMap;
    private String add;
    private int point;
    private TextView toolbar_title;
    private TextView text_mission;
    private final String SAVED_BUNDLE_TAG = "saved_bundle";
    private boolean isChecked;
    private ProgressBar progres_bar;
    private HashMap<Integer, LatLng[]> customer;
    private MarkerOptions markerOptions;
    private View view_dialog;
    private Button start_button;
    private LinearLayout linear_start;
    private List<NearBy> movieList = new ArrayList<>();
    private RecyclerView recyclerView1;
    private NearByAdapter mAdapter1;
    private Button button_others;
    private ImageView image_close;
    private TextView text_close;
    private Polyline polyline;
    public static final PatternItem GAP = new Gap(20);
    public static final PatternItem DASH = new Dash(20);
    public static final List<PatternItem> PATTERN_POLYGON_ALPHA = Arrays.asList(GAP, DASH);
    private Marker marker;
    MapRipple mapRipple;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //setup the listener for the fragment B
        ViewModelProviders.of(getActivity()).get(PagerAgentViewModel.class).getMessageContainerB().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(@Nullable Integer msg) {
                if(msg !=500) {

                    isChecked =false;

                    linear_start.setVisibility(View.GONE);
//                    prepareMovieData();
                    recyclerView1.setVisibility(View.VISIBLE);
                    image_close.setVisibility(View.VISIBLE);
                    text_close.setVisibility(View.VISIBLE);
                    button_others.setVisibility(View.INVISIBLE);
                    Log.i("Marker", "" + msg);
                    point = msg;

                    marker(customer.get(msg+1), msg);
//                    Toast.makeText(getContext(), "" + msg, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public static FirstFragment newInstance() {

        FirstFragment fragment = new FirstFragment();
        return fragment;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        Log.i(TAG, "onSaveInstanceState: ");
        outState.putBoolean(SAVED_BUNDLE_TAG, isChecked);
        outState.putInt("lantitude", point);
        outState.putString("Address", add);
        linear_start.setVisibility(View.GONE);
        super.onViewStateRestored(outState);
    }

    @SuppressLint("MissingPermission")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.i(TAG, "onCreateView: FirstFragment");
        View view = inflater.inflate(R.layout.fragment_one, container, false);
        view_dialog = inflater.inflate(R.layout.bottom_sheet, container, false);


        SupportMapFragment mapFragment = (SupportMapFragment) this.getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        final CheckBox cbCheckBox = (CheckBox) view.findViewById(R.id.checkBox);

        progres_bar = (ProgressBar) view.findViewById(R.id.progressBar);
        toolbar_title = (TextView) view.findViewById(R.id.toolbar_title);
        text_mission = (TextView) view.findViewById(R.id.text_mission);
        start_button = (Button) view.findViewById(R.id.start_button);
        linear_start = (LinearLayout) view.findViewById(R.id.layout_start);

        button_others = (Button) view.findViewById(R.id.button_others);
        image_close = (ImageView) view.findViewById(R.id.image_close);
        text_close = (TextView) view.findViewById(R.id.text_close);

        text_close.setTypeface(G.iransens_light);
        start_button.setTypeface(G.iransens_light);
        text_mission.setTypeface(G.iransens_light);

        text_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideRecycelerView();
            }
        });

        image_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideRecycelerView();
            }
        });
        start_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewPager viewPager = (ViewPager) getActivity().findViewById(R.id.container);
                viewPager.setCurrentItem(1);
            }
        });

        button_others.setTypeface(G.iransens_light);
        button_others.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recyclerView1.setVisibility(View.VISIBLE);
                image_close.setVisibility(View.VISIBLE);
                text_close.setVisibility(View.VISIBLE);
                button_others.setVisibility(View.INVISIBLE);
            }
        });

        customer = new HashMap<>();

        LatLng[] lat1 = new LatLng[2];
        lat1[0] = new LatLng(37.377924146,49.804278743);
        lat1[1] = new LatLng(37.390960185,49.806601787);
//        lat1[2] = new LatLng(37.377924146,49.804278743);

        LatLng[] lat2 = new LatLng[1];
        lat2[0] = new LatLng(37.377865976, 49.804316042);
//        lat2[1] = new LatLng(37.377924146,49.804278743);

        LatLng[] lat3 = new LatLng[1];
        lat3[0] = new LatLng(37.387742121,49.807384573);

        LatLng[] lat4 = new LatLng[4];
        lat4[0] = new LatLng(37.379179671,49.801236698);
        lat4[1] = new LatLng(37.390125766,49.806626681);
        lat4[2] = new LatLng(37.388876444,49.810535247);
        lat4[3] = new LatLng(37.390486021, 49.807777517);
//        lat4[4] = new LatLng(37.377924146,49.804278743);

        customer.put(1, lat1);
        customer.put(2, lat2);
        customer.put(3, lat3);
        customer.put(4, lat4);






//        Location myLocation = mMap.getMyLocation();  //Nullpointer exception.........
//        LatLng myLatLng = new LatLng(myLocation.getLatitude(),
//                myLocation.getLongitude());
//
//        try {
//
//            Geocoder geo = new Geocoder(getActivity().getApplicationContext(), new Locale("fa"));
//            List<Address> addresses = null;
//            try {
//                addresses = geo.getFromLocation(myLatLng.latitude, myLatLng.longitude, 1);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            if (addresses.isEmpty()) {
//                toolbar_title.setText("Waiting for Location");
//            }
//            else {
//                if (addresses.size() > 0) {
//                    toolbar_title.setText(addresses.get(0).getFeatureName() + ", " + addresses.get(0).getLocality() +", " + addresses.get(0).getAdminArea() + ", " + addresses.get(0).getCountryName());
//                }
//            }
//        } catch (Exception e) {
//        }

        if (savedInstanceState != null) {
            Log.d(TAG, "savedInstanceState: true");
            isChecked = savedInstanceState.getBoolean(SAVED_BUNDLE_TAG, false);
            add = savedInstanceState.getString("Address", "Iran");
            point = savedInstanceState.getInt("latitude", 0);
            toolbar_title.setText(add);
            cbCheckBox.setChecked(isChecked);
        }
        cbCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                Log.d(TAG, "onCheckedChanged: " + b);
                isChecked = b;
            }

        });

        recyclerView1 = (RecyclerView) view.findViewById(R.id.recycler_view1);
//
        mAdapter1 = new NearByAdapter(movieList, FirstFragment.this);

        mAdapter1.setOnItemClickListener(new NearByAdapter.ClickListener() {
            @Override
            public void onItemClick ( int position, View v){

                LatLng latlongClick = movieList.get(position).getYear();
                markAndRipple(latlongClick);



            }
        });
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        recyclerView1.setLayoutManager(mLayoutManager);
        recyclerView1.setItemAnimator(new DefaultItemAnimator());
        recyclerView1.setAdapter(mAdapter1);
//




        return view;
    }

    private void markAndRipple(LatLng mLatlong) {
        if(marker != null) {
            marker.remove();
        }
        markerOptions = new MarkerOptions();

        // Setting the position for the marker
        markerOptions.position(mLatlong);


        Drawable circleDrawable = getResources().getDrawable(R.drawable.icon_marker3);
        BitmapDescriptor markerIcon = getMarkerIconFromDrawable(circleDrawable);

        // Setting the title for the marker.
        // This will be displayed on taping the marker
        markerOptions.title(mLatlong.latitude + " : " + mLatlong.longitude);
        markerOptions.icon(markerIcon);



        // Placing a marker on the touched position
        marker = mMap.addMarker(markerOptions);

        zoomInmap(mLatlong);

        if(mapRipple != null) {
            if(mapRipple.isAnimationRunning()) {
                Log.d("AnimationRipple" , "Running");
                mapRipple.stopRippleMapAnimation();
                LatLng fake_longlat = new LatLng(34.0522, 118.2437);
                mapRipple.withLatLng(mLatlong);
            }

        } else {
            mapRipple = new MapRipple(mMap, mLatlong, context);
        }

        mapRipple.withNumberOfRipples(5);
        mapRipple.withFillColor(context.getResources().getColor(R.color.colorPrimaryLight));
        mapRipple.withStrokeColor(context.getResources().getColor(R.color.colorPrimary));
        mapRipple.withStrokewidth(1);      // 10dp
        mapRipple.withDistance(200);      // 2000 metres radius
        mapRipple.withRippleDuration(3000);    //12000ms
        mapRipple.withTransparency(0.8f);
        mapRipple.startRippleMapAnimation();      //in onMapReadyCallBack
    }


    private void prepareMovieData(LatLng latLng, int msg, int count){
        String[] name = {getString(R.string.customer_1), getString(R.string.customer_2), getString(R.string.customer_3), getString(R.string.customer_4)};
        NearBy task = new NearBy(name[msg], getString(R.string.num_of_land) + "" + count, latLng);

        if (!contains(movieList, name[msg],count)) {
            movieList.add(task);
        }


//        mAdapter1.notifyDataSetChanged();
    }

    boolean contains(List<NearBy> list, String name, int count) {
        for (NearBy item : list) {
            if (item.getTitle().equals(name) && item.getGenre().equals(getString(R.string.num_of_land) + "" + count)) {
                return true;
            }
        }
        return false;
    }
    public void hideRecycelerView() {
        recyclerView1.setVisibility(View.INVISIBLE);
        button_others.setVisibility(View.VISIBLE);
        image_close.setVisibility(View.INVISIBLE);
        text_close.setVisibility(View.INVISIBLE);
    }




    @Override
    public void onMapReady(GoogleMap map) {
        if (mMap != null) {
            return;
        }
        mMap = map;
        startDemo();
    }

    public void marker(LatLng[] latLng, int msg){
        int count = 0;
        for(LatLng latlong : latLng) {

            count++;
            //prepare data
            prepareMovieData(latlong, msg, count);

        }

        markAndRipple(latLng[0]);

        mAdapter1.notifyDataSetChanged();

        zoomInmap(latLng[0]);

    }

    // funtion that zoom in places:
    private void zoomInmap(LatLng latLngs) {

        LatLng coordinate = new LatLng(latLngs.latitude, latLngs.longitude); //Store these lat lng values somewhere. These should be constant.
        CameraUpdate location = CameraUpdateFactory.newLatLngZoom(
                coordinate, 15);
        mMap.animateCamera(location);
        }

    //Direction to destionation of each planet.
    public void requestDirection(LatLng destination) {

//        MapboxNavigation navigation = new MapboxNavigation(G.context, "pk.eyJ1IjoiY2hlZXRhaHNndW4iLCJhIjoiY2pqY3A3bnFvM3NubzNyczJhcTRpa3RreiJ9.dnTdkRJK_WY_JnBvH_JM1g");

//
        LatLng orogin = new LatLng(37.387979, 49.798806);
        markAndRipple(destination);
//        LatLng destination = new LatLng(37.388876444,49.810535247);
//        Snackbar.make(btnRequestDirection, "Direction Requesting...", Snackbar.LENGTH_SHORT).show();
        Drawable circleDrawable = getResources().getDrawable(R.drawable.placeholder);
        BitmapDescriptor markerIcon = getMarkerIconFromDrawable(circleDrawable);

        mMap.addMarker(new MarkerOptions().position(orogin).title(getString(R.string.u_place))
                .icon(markerIcon));

//        mMap.addMarker(new MarkerOptions().position(destination));

        GoogleDirection.withServerKey("AIzaSyDZOj1QSw4MM4RzPdUEeNTLEhlntcyfMqw")
                .from(orogin)
                .to(destination)
                .transportMode(TransportMode.DRIVING)
                .execute(new DirectionCallback() {
                    @Override
                    public void onDirectionSuccess(Direction direction, String rawBody) {
                        if(direction.isOK()){
                            if(polyline != null) {
                                polyline.remove();
                            }

                            Route route = direction.getRouteList().get(0);
                            ArrayList<LatLng> directionPositionList = route.getLegList().get(0).getDirectionPoint();
                            polyline = mMap.addPolyline(DirectionConverter.createPolyline(getActivity(), directionPositionList, 5, context.getResources().getColor(R.color.colorPrimary)).pattern(PATTERN_POLYGON_ALPHA));
                            setCameraWithCoordinationBounds(route);
                        }
                    }

                    @Override
                    public void onDirectionFailure(Throwable t) {

                    }
                });
    }



    private void setCameraWithCoordinationBounds(Route route) {
        LatLng southwest = route.getBound().getSouthwestCoordination().getCoordination();
        LatLng northeast = route.getBound().getNortheastCoordination().getCoordination();
        LatLngBounds bounds = new LatLngBounds(southwest, northeast);
        mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100));
    }

    private BitmapDescriptor getMarkerIconFromDrawable(Drawable drawable) {
        int width =128, height =128;
        Canvas canvas = new Canvas();
        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        canvas.setBitmap(bitmap);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        drawable.draw(canvas);
        Bitmap smallMarker = Bitmap.createScaledBitmap(bitmap, width, height, false);
        return BitmapDescriptorFactory.fromBitmap(smallMarker);
    }


    public void getLocation(double lat, double lng) {
        Geocoder geocoder = new Geocoder(getActivity(),new Locale("fa"));

        try {
            List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
            Address obj = addresses.get(0);
            add = obj.getAddressLine(0);

            Log.e("IGA", "Address" + add);
            toolbar_title.setText(add);
            toolbar_title.setTypeface(G.iransens_light);
            progres_bar.setVisibility(View.GONE);
            toolbar_title.setVisibility(View.VISIBLE);



        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }

    }

//    // Check for permission to access Location
//    private boolean checkPermission() {
//        Log.d(TAG, "checkPermission()");
//        // Ask for permission if it wasn't granted yet
//        return (ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION)
//                == PackageManager.PERMISSION_GRANTED );
//    }
//    // Asks for permission
//    private void askPermission() {
//        Log.d(TAG, "askPermission()");
//        ActivityCompat.requestPermissions(
//                getActivity(),
//                new String[] { android.Manifest.permission.ACCESS_FINE_LOCATION },
//                REQ_PERMISSION
//        );
//    }
//
//    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        Log.d(TAG, "onRequestPermissionsResult()");
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        switch (requestCode) {
//            case REQ_PERMISSION: {
//                if (grantResults.length > 0
//                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    // Permission granted
//                    if (checkPermission())
//                        mMap.setMyLocationEnabled(true);
//
//                } else {
//                    // Permission denied
//
//                }
//                break;
//            }
//        }
//    }





    protected GoogleMap getMap() {
        return mMap;
    }

    public void startDemo () {
        try {
            mMap = getMap();
            retrieveFileFromResource();
            mMap.setOnMarkerClickListener(this);
            try {
                boolean success = mMap.setMapStyle(
                        MapStyleOptions.loadRawResourceStyle(context, R.raw.style_json));
                if (!success) {
                    // Handle map style load failure
                }
            } catch (Resources.NotFoundException e) {
                // Oops, looks like the map style resource couldn't be found!
            }
            //retrieveFileFromUrl();
        } catch (Exception e) {
            Log.e("Exception caught", e.toString());
        }
    }

    private void retrieveFileFromResource() {
        try {
            KmlLayer kmlLayer = new KmlLayer(mMap, R.raw.campus, getActivity().getApplicationContext());
            kmlLayer.addLayerToMap();
            kmlLayer.setOnFeatureClickListener(new KmlLayer.OnFeatureClickListener() {

                // open new intent activity for making campute for each item.
                @Override
                public void onFeatureClick(Feature feature) {


//                    DialogBottom dialog = DialogBottom.newInstance("");
//
//                    dialog.show(getActivity().getFragmentManager(), "show");


                  /*  Toast.makeText(KmlDemoActivity.this,
                            "Feature clicked: " + feature.getId(),
                            Toast.LENGTH_SHORT).show(); */
//                  final Feature feature_new = feature;
//
//                    Context ctx= getActivity();
//                    new MaterialDialog.Builder(ctx)
//                            .title("test")
//                            .customView(R.layout.bottom_sheet, false)
//                            .show();

//                    Intent myIntent = new Intent(getActivity(), ContentActivity.class);
//                    myIntent.putExtra("Address", feature.getId()); //Optional parameters
//                    startActivity(myIntent);
                }
            });
            moveCameraToKml(kmlLayer);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        }
    }

//    private void retrieveFileFromUrl() {
//        new DownloadKmlFile(getString(R.string.kml_url)).execute();
//    }

    private void moveCameraToKml(KmlLayer kmlLayer) {
        //Retrieve the first container in the KML layer
        KmlContainer container = kmlLayer.getContainers().iterator().next();
        //Retrieve a nested container within the first container
        container = container.getContainers().iterator().next();
        //Retrieve the first placemark in the nested container
        KmlPlacemark placemark = container.getPlacemarks().iterator().next();
        //Retrieve a polygon object in a placemark
        KmlPolygon polygon = (KmlPolygon) placemark.getGeometry();
        //Create LatLngBounds of the outer coordinates of the polygon
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        int counter = 0;
        for (LatLng latLng : polygon.getOuterBoundaryCoordinates()) {
            Log.i("LatLong", String.valueOf(latLng));
            builder.include(latLng);
            if(counter==0) {
                getLocation(latLng.latitude,latLng.longitude);

                counter++;
            }
        }

        int width = getResources().getDisplayMetrics().widthPixels;
        int height = getResources().getDisplayMetrics().heightPixels;
        getMap().moveCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), width/8, height/8, 20));
    }

    @Override
    public boolean onMarkerClick(Marker marker) {

        LatLng orogin = new LatLng(37.387979, 49.798806);
            if(!(marker.getPosition().equals(orogin))) {

                String[] name = new String[]{getString(R.string.customer_1), getString(R.string.customer_2), getString(R.string.customer_3), getString(R.string.customer_4)};

                for (NearBy item : movieList) {
                    if (item.getYear().equals(marker.getPosition())) {
                        DialogBottom dialog = DialogBottom.newInstance(item.getTitle(), item.getGenre());
                        dialog.show(getActivity().getFragmentManager(), "show");
                    }
                }

            }
        return false;
    }



//    private class DownloadKmlFile extends AsyncTask<String, Void, byte[]> {
//        private final String mUrl;
//
//        public DownloadKmlFile(String url) {
//            mUrl = url;
//        }
//
//        protected byte[] doInBackground(String... params) {
//            try {
//                InputStream is =  new URL(mUrl).openStream();
//                ByteArrayOutputStream buffer = new ByteArrayOutputStream();
//                int nRead;
//                byte[] data = new byte[16384];
//                while ((nRead = is.read(data, 0, data.length)) != -1) {
//                    buffer.write(data, 0, nRead);
//                }
//                buffer.flush();
//                return buffer.toByteArray();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            return null;
//        }
//
//        protected void onPostExecute(byte[] byteArr) {
//            try {
//                KmlLayer kmlLayer = new KmlLayer(mMap, new ByteArrayInputStream(byteArr),
//                        getApplicationContext());
//                kmlLayer.addLayerToMap();
//                kmlLayer.setOnFeatureClickListener(new KmlLayer.OnFeatureClickListener() {
//
//                    // open new intent activity for making campute for each item.
//                    @Override
//                    public void onFeatureClick(Feature feature) {
////                        Toast.makeText(Main3Activity.this,
////                                "Feature clicked: " + feature.getId(),
////                                Toast.LENGTH_SHORT).show();
//                    }
//                });
//                moveCameraToKml(kmlLayer);
//            } catch (XmlPullParserException e) {
//                e.printStackTrace();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//    }


}


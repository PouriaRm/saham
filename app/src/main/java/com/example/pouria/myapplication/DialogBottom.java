package com.example.pouria.myapplication;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;

import static com.example.pouria.myapplication.G.context;

/**
 * Created by Pouria on 6/11/2018.
 */

public class DialogBottom extends DialogFragment {


    public static DialogBottom newInstance(String title, String genere) {
        DialogBottom dialogBottom = new DialogBottom();
        Bundle args = new Bundle();
        args.putString("title", title);
        args.putString("genere", genere);
        dialogBottom.setArguments(args);
        return dialogBottom;

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        return inflater.inflate(R.layout.bottom_sheet, container);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        String[] name = new String[]{getString(R.string.customer_1), getString(R.string.customer_2), getString(R.string.customer_3), getString(R.string.customer_4)};
        int[] num = {2,1,1,4};
        int count = 0;
        TextView textView1 = (TextView) view.findViewById(R.id.dialog_text_name);
        TextView textView2 = (TextView) view.findViewById(R.id.dialog_text_num);
        TextView textView3 = (TextView) view.findViewById(R.id.dialog_text_operator);
        TextView textView4 = (TextView) view.findViewById(R.id.dialog_text_vaziat);
        String title = getArguments().getString("title", "Enter title");
        String genere = getArguments().getString("genere", "Enter title");
        Log.i("Title", title);
        textView1.setText(title);
        textView1.setTypeface(G.iransens_light);
        for(int i=0; i<name.length; i++){
        if(title.equals(name[i])) {
            count =num[i];
            }

        }
        String con = genere + getString(R.string.num_az) +""+ count;
        textView2.setText(con);
        textView2.setTypeface(G.iransens_light);
        textView3.setTypeface(G.iransens_light);
        textView4.setTypeface(G.iransens_light);

        Button button = (Button) view.findViewById(R.id.button_analayse);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(getActivity(), ContentActivity.class);
                myIntent.putExtra("Address", "Rasht"); //Optional parameters
                startActivity(myIntent);
            }
        });
        button.setTypeface(G.iransens_light);


        // make Dialog Bottom of Layout & Fix Display Size.
        Window window = getDialog().getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.BOTTOM;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);

        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();

        int width = display.getWidth();  // deprecated
        int height = display.getHeight();  // deprecated

        LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.linearlayout);

        ViewGroup.LayoutParams params = linearLayout.getLayoutParams();
        // Changes the height and width to the specified *pixels*
        params.height = height/3;
        params.width = width -100;
        linearLayout.setLayoutParams(params);
//                getDialog().getWindow().setSoftInputMode(
//                WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

    }


}

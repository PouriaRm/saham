package com.example.pouria.myapplication;

/**
 * Created by Pouria on 5/18/2018.
 */

import android.app.Notification;
import android.app.NotificationManager;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

public class Main3Activity extends AppCompatActivity
        implements BottomNavigationView.OnNavigationItemSelectedListener, SecondFragment.onSetPointListner {

    private ViewPager mViewPager;
    private PagerAgentViewModel pagerAgentViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        //mTextMessage = (TextView) findViewById(R.id.message);
        final BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(this);

        TextView toolbar_title = (TextView) findViewById(R.id.toolbar_title1);
        toolbar_title.setTypeface(G.sultanadnan);

        mViewPager = (ViewPager) findViewById(R.id.container);
        //mViewPager.setOffscreenPageLimit(1);
        DemoFragmentAdapter adapterViewPager = new DemoFragmentAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(adapterViewPager);
        pagerAgentViewModel = ViewModelProviders.of(this).get(PagerAgentViewModel.class);
        pagerAgentViewModel.init();
        //mViewPager.setCurrentItem(0);

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            public void onPageScrollStateChanged(int state) {

            }

            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            public void onPageSelected(int position) {
                navigation.getMenu().getItem(position).setChecked(true);
            }
        });

        if (savedInstanceState == null) {
            onNavigationItemSelected(navigation.getMenu().findItem(R.id.navigation_home));
        }

        mViewPager.setOffscreenPageLimit(3);



//        new Handler().postDelayed(new Runnable() {
//
//            /*
//             * Showing splash screen with a timer. This will be useful when you
//             * want to show case your app logo / company
//             */
//
//            @Override
//            public void run() {
//
//                NotificationManager notificationManager =
//                        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//                int notifyId = 1;
//                String channelId = "some_channel_id";
//
//                Notification notification = new Notification.Builder(Main3Activity.this)
//                        .setContentTitle("Some Message")
//                        .setContentText("You've received new messages!")
//                        .setSmallIcon(R.drawable.path_back)
//                        .build();
//
//                notificationManager.notify(notifyId, notification);
//
//            }
//        }, 10000);

    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case R.id.navigation_home:
                mViewPager.setCurrentItem(0);
                return true;
            case R.id.navigation_dashboard:
                mViewPager.setCurrentItem(1);
                return true;
            case R.id.navigation_notifications:
                mViewPager.setCurrentItem(2);
                return true;
            default:
                return false;
        }

    }

    @Override
    public void setPoint(String point) {

        FirstFragment firstFragment = new FirstFragment();
        String tag = makeFragmentName(mViewPager.getId(), 2);
//        firstFragment.getFragmentManager().findFragmentById(R.id.container);
//        firstFragment.updatePoint(point);
    }

    public static String makeFragmentName(int containerViewId, long id) {
        return "android:switcher:" + containerViewId + ":" + id;
    }


}
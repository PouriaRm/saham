package com.example.pouria.myapplication;

/**
 * Created by Pouria on 5/25/2018.
 */
import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.pkmmte.view.CircularImageView;

import java.util.List;



public class TaskAdapter extends RecyclerView.Adapter<TaskAdapter.MyViewHolder> {

    private List<Task> moviesList;
    private Context context;

    private static ClickListener clickListener;

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener  {
        public TextView title, type, genre;
        public LinearLayout linearlayout;
        public RelativeLayout realtivelayout;
        public ImageView image_item;
        public ImageView circularImageView;
        public RelativeLayout relativeLayout;


        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            genre = (TextView) view.findViewById(R.id.genre);
            type = (TextView) view.findViewById(R.id.type);
            linearlayout = (LinearLayout) view.findViewById(R.id.linearlayout);
            realtivelayout = (RelativeLayout) view.findViewById(R.id.relativelayout);
//            image_item = (ImageView) view.findViewById(R.id.image_item);
            circularImageView = (ImageView) view.findViewById(R.id.image_circle);
            realtivelayout = (RelativeLayout) view.findViewById(R.id.relativelayout);
            view.setOnClickListener(this);
//            year = (TextView) view.findViewById(R.id.year);
        }

        @Override
        public void onClick(View v) {
            clickListener.onItemClick(getAdapterPosition(), v);
        }
    }


    public TaskAdapter(List<Task> moviesList) {
        this.moviesList = moviesList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_second_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Task movie = moviesList.get(position);
        holder.title.setText(movie.getTitle());
        holder.title.setTypeface(G.iransens_light);
        holder.genre.setText(movie.getGenre());
        holder.genre.setTypeface(G.iransens_light);
        holder.type.setText(movie.getYear());
        holder.type.setTypeface(G.iransens_light);


        if(position > 3) {
//            holder.image_item.setImageResource(getImage(position%3));
            Glide.with(G.context).load(getImage(position%3)).into(holder.circularImageView);
        } else {
            Glide.with(G.context).load(getImage(position)).into(holder.circularImageView);
//            holder.image_item.setImageResource(getImage(position));
//            holder.circleImageView.setImageResource(getImage(position));
        }

        if((position%2) == 0) {
            holder.linearlayout.setBackgroundResource(R.color.colorAccent);
            holder.realtivelayout.setBackgroundResource(R.color.colorAccent);
        }


//        holder.year.setText(movie.getYear());
    }

    private int getImage(int position) {

        int[] images = {
                R.drawable.one,
                R.drawable.two,
                R.drawable.tree,
                R.drawable.four
        };

       return images[position];
    }


    @Override
    public int getItemCount() {
        return moviesList.size();
    }
    public void setOnItemClickListener(ClickListener clickListener) {
        TaskAdapter.clickListener = clickListener;
    }
    public interface ClickListener {
        void onItemClick(int position, View v);
    }
}

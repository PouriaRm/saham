package com.example.pouria.myapplication;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

/**
 * Created by Pouria on 2/9/2018.
 */

public class ContentActivity extends AppCompatActivity {

    static final int REQUEST_IMAGE_CAPTURE = 1;
    private Bitmap mImageBitmap;
    private String mCurrentPhotoPath;
    private ImageView mImageView;
    private Button takePictureButton;
    private Uri file;
    private static final int CAMERA_REQUEST = 1888;
    private static final int MY_CAMERA_PERMISSION_CODE = 100;
    private ImageView mIconphoto;
    private Button button_arzeyabi;
    private Toolbar toolbar1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_activity);

        final int random_area = new Random().nextInt(15000) + 20;
        final int random_sabt = new Random().nextInt(15000) + 20;
        final int random_month = new Random().nextInt(3) + 1;
        final int random_day = new Random().nextInt(28) + 1;
        final int random_num = new Random().nextInt(20) + 1;

        // Making notification bar transparent
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }

        changeStatusBarColor();
        toolbar1 = (Toolbar) findViewById(R.id.toolbar1);
        setSupportActionBar(toolbar1);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        //toolbar.setNavigationIcon(R.drawable.ic_toolbar);
        toolbar1.setTitle("");
        toolbar1.setSubtitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        Intent intent = getIntent();
        String value = intent.getStringExtra("Address"); //if it's a string you stored.

        TextView arzeyabi_title = (TextView) findViewById(R.id.arzyabi_title);
        arzeyabi_title.setTypeface(G.sultanadnan);
        TextView text_owner = (TextView) findViewById(R.id.text_owner1);
        text_owner.setText("1397/" + random_month + "/" + random_day);
        text_owner.setTypeface(G.iransens_light);
        TextView text_num = (TextView) findViewById(R.id.text_num1);
        text_num.setText("" + random_num);
        text_num.setTypeface(G.iransens_light);
        TextView text_area = (TextView) findViewById(R.id.text_area1);
        text_area.setTypeface(G.iransens_light);
        text_area.setText("" + random_area);
        TextView text_owner2 = (TextView) findViewById(R.id.text_owner2);
        text_owner2.setTypeface(G.iransens_light);
        TextView text_num2 = (TextView) findViewById(R.id.text_num2);
        text_num2.setTypeface(G.iransens_light);
        TextView text_area2 = (TextView) findViewById(R.id.text_area2);
        text_area2.setTypeface(G.iransens_light);
        TextView text_sabt1 = (TextView) findViewById(R.id.text_sabt1);
        text_sabt1.setTypeface(G.iransens_light);
        text_sabt1.setText("" + random_sabt);
        TextView text_sabt2 = (TextView) findViewById(R.id.text_sabt2);
        text_sabt2.setTypeface(G.iransens_light);


        takePictureButton = (Button) findViewById(R.id.button_image);
        mImageView = (ImageView) findViewById(R.id.camera);
        mIconphoto = (ImageView) findViewById(R.id.camera_icon);
        button_arzeyabi = (Button) findViewById(R.id.button_arzyabi);
        button_arzeyabi.setTypeface(G.iransens_light);
        takePictureButton.setTypeface(G.iransens_light);

        button_arzeyabi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(ContentActivity.this, getString(R.string.sucess), Toast.LENGTH_SHORT).show();
            }
        });
        String colors[] = {getString(R.string.spinner_item_1),getString(R.string.spinner_item_2),getString(R.string.spinner_item_3),getString(R.string.spinner_item_4),getString(R.string.spinner_item_5), getString(R.string.spinner_item_6),getString(R.string.spinner_item_7)};

// Selection of the spinner
        Spinner spinner = (Spinner) findViewById(R.id.spinner);

// Application of the Array to the Spinner
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this,   android.R.layout.simple_spinner_item, colors);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        spinner.setAdapter(spinnerArrayAdapter);

        takePictureButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                        mIconphoto.setVisibility(View.GONE);
                        mImageView.setVisibility(View.VISIBLE
                        );
                        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(cameraIntent, CAMERA_REQUEST);
                    }

            }
        });



    }



    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            mImageView.setImageBitmap(photo);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }
}


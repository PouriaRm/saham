package com.example.pouria.myapplication;

/**
 * Created by Pouria on 5/18/2018.
 */

import android.graphics.Paint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.Chart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;


public class ThirdFragment extends Fragment {
    private static final String TAG = ThirdFragment.class.getSimpleName();

    private final String SAVED_BUNDLE_TAG = "saved_bundle";
    private boolean isChecked;

    public static ThirdFragment newInstance() {
        ThirdFragment fragment = new ThirdFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
//        Log.i(TAG, "onSaveInstanceState: ");
//        outState.putBoolean(SAVED_BUNDLE_TAG, isChecked);
        super.onViewStateRestored(outState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.i(TAG, "onCreateView: FirstFragment");
        View view = inflater.inflate(R.layout.fragment_third, container, false);
//        final CheckBox cbCheckBox = (CheckBox) view.findViewById(R.id.checkBox);
        TextView username = (TextView) view.findViewById(R.id.username);
        TextView textView1 = (TextView) view.findViewById(R.id.textView1);
        TextView textView2 = (TextView) view.findViewById(R.id.textView2);
        TextView textView3 = (TextView) view.findViewById(R.id.textView3);
        TextView textView4 = (TextView) view.findViewById(R.id.textView4);
        username.setTypeface(G.iransens_light);
        textView1.setTypeface(G.iransens_light);
        textView2.setTypeface(G.iransens_light);
        textView3.setTypeface(G.iransens_light);
        textView4.setTypeface(G.iransens_light);

//        ImageView image_profile = (ImageView) view.findViewById(R.id.profile_image);
//        ImageView image_shears = (ImageView) view.findViewById(R.id.image_shears);
//        ImageView image_check_mark = (ImageView) view.findViewById(R.id.image_check_mark);
//        ImageView image_envelope = (ImageView) view.findViewById(R.id.image_envelope);
//        ImageView image_purse = (ImageView) view.findViewById(R.id.image_purse);
//
//        Glide.with(G.context).load(R.id.profile_image).into(image_profile);
//        Glide.with(G.context).load(R.id.image_shears).into(image_shears);
//        Glide.with(G.context).load(R.id.image_check_mark).into(image_check_mark);
//        Glide.with(G.context).load(R.id.image_envelope).into(image_envelope);
//        Glide.with(G.context).load(R.id.image_purse).into(image_purse);

        if (savedInstanceState != null) {
//            Log.d(TAG, "savedInstanceState: true");
//            isChecked = savedInstanceState.getBoolean(SAVED_BUNDLE_TAG, false);
//            cbCheckBox.setChecked(isChecked);
        }
//        cbCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
//                Log.d(TAG, "onCheckedChanged: " + b);
//                isChecked = b;
//            }
//        });

        ArrayList<BarEntry> entries = new ArrayList<>();
        entries.add(new BarEntry(4f, 0));
        entries.add(new BarEntry(8f, 1));
        entries.add(new BarEntry(6f, 2));
        entries.add(new BarEntry(12f, 3));
        entries.add(new BarEntry(18f, 4));
        entries.add(new BarEntry(9f, 5));

        BarDataSet dataset = new BarDataSet(entries, getString(R.string.panish));

        ArrayList<String> labels = new ArrayList<String>();
        labels.add("January");
        labels.add("February");
        labels.add("March");
        labels.add("April");
        labels.add("May");
        labels.add("June");

        BarChart chart = new BarChart(getActivity().getBaseContext());
        BarChart layout = (BarChart) view.findViewById(R.id.piechart);
        layout.addView(chart);

        chart.setDrawBarShadow(false);
        chart.setDrawValueAboveBar(false);
        chart.getDescription().setEnabled(false);
        chart.setDrawGridBackground(false);
        chart.setNoDataText(" ");
        Paint p = chart.getPaint(Chart.PAINT_INFO);
        p.setColor(getResources().getColor(android.R.color.white));



        IAxisValueFormatter xAxisFormatter = new DayAxisValueFormatter(chart);

        XAxis xAxis = chart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setTypeface(G.iransens_light);
        xAxis.setDrawGridLines(false);
        xAxis.setGranularity(1f); // only intervals of 1 day
        xAxis.setLabelCount(7);
        xAxis.setValueFormatter(xAxisFormatter);

        IAxisValueFormatter custom = new MyAxisValueFormatter();

        YAxis leftAxis = chart.getAxisLeft();
        leftAxis.setTypeface(G.iransens_light);
        leftAxis.setLabelCount(8, false);
        leftAxis.setValueFormatter(custom);
        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        leftAxis.setSpaceTop(15f);
        leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)


        Legend l = chart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);
        l.setForm(Legend.LegendForm.SQUARE);
        l.setFormSize(9f);
        l.setTextSize(11f);

        BarData data = new BarData(dataset);
        chart.setData(data);

        dataset.setColors(ColorTemplate.JOYFUL_COLORS);
        chart.animateY(5000);

        return view;
    }


}

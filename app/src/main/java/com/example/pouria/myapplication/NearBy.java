package com.example.pouria.myapplication;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Pouria on 5/25/2018.
 */

public class NearBy {
    private String title, genre;
    LatLng latlng;

    public NearBy() {
    }

    public NearBy(String title, String genre, LatLng year) {
        this.title = title;
        this.genre = genre;
        this.latlng = year;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String name) {
        this.title = name;
    }

    public LatLng getYear() {
        return latlng;
    }

    public void setYear(String year) {
        this.latlng = latlng;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

}

package com.example.pouria.myapplication;

/**
 * Created by Pouria on 5/18/2018.
 */

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


public class SecondFragment extends Fragment {
    private static final String TAG = SecondFragment.class.getSimpleName();

    private final String SAVED_BUNDLE_TAG = "saved_bundle";
    private boolean isChecked;

    private List<Task> movieList = new ArrayList<>();
    private RecyclerView recyclerView;
    private TaskAdapter mAdapter;
    private onSetPointListner onSetPointListner;

    public static SecondFragment newInstance() {
        SecondFragment fragment = new SecondFragment();
        return fragment;
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
//        Log.i(TAG, "onSaveInstanceState: ");
//        outState.putBoolean(SAVED_BUNDLE_TAG, isChecked);
        super.onViewStateRestored(outState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        Log.i(TAG, "onCreateView: FirstFragment");
        View view = inflater.inflate(R.layout.fragment_second, container, false);
//        final CheckBox cbCheckBox = (CheckBox) view.findViewById(R.id.checkBox);
//
//        if (savedInstanceState != null) {
//            Log.d(TAG, "savedInstanceState: true");
//            isChecked = savedInstanceState.getBoolean(SAVED_BUNDLE_TAG, false);
//            cbCheckBox.setChecked(isChecked);
//        }
//        cbCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
//                Log.d(TAG, "onCheckedChanged: " + b);
//                isChecked = b;
//            }
//        });
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);

        mAdapter = new TaskAdapter(movieList);
        mAdapter.setOnItemClickListener(new TaskAdapter.ClickListener() {
            @Override
            public void onItemClick(int position, View v) {
//                String channelId = "some_channel_id";


                ViewPager viewPager = (ViewPager) getActivity().findViewById(R.id.container);
                Log.i("Click", "Clickede!" + position);
//                FirstFragment firstFragment = new FirstFragment();
//                FragmentManager transaction = getFragmentManager();
//                transaction.beginTransaction().replace(R.id.main_content, firstFragment).commit();
//                transaction.replace(viewPager, firstFragment).commit();
//                Bundle bundle = new Bundle();
//                bundle.putString("latitude", "Rasht");
//                bundle.putString("longitude", longitude);
//                bundle.putString("board_id", board_id);
//                firstFragment.setArguments(bundle);
//                onSetPointListner.setPoint("Rasht");
                ViewModelProviders.of(getActivity()).get(PagerAgentViewModel.class).sendMessageToB(position);
                viewPager.setCurrentItem(0);
            }
        });
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);



        prepareMovieData();
        delayPrepareMoviewData();

        return view;
    }

    private void prepareMovieData(){
        Task task = new Task(getString(R.string.customer_1), getString(R.string.location_1), getString(R.string.type_1));
        movieList.add(task);

        task = new Task(getString(R.string.customer_2), getString(R.string.location_1), getString(R.string.type_2));
        movieList.add(task);

        task = new Task(getString(R.string.customer_3), getString(R.string.location_1), getString(R.string.type_3));
        movieList.add(task);



        mAdapter.notifyDataSetChanged();
    }

    private  void  delayPrepareMoviewData() {
        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {

                NotificationManager notificationManager =
                        (NotificationManager) G.context.getSystemService(Context.NOTIFICATION_SERVICE);
                int notifyId = 1;
                String channelId = "some_channel_id";

                Notification notification = new Notification.Builder(getActivity())
                        .setContentTitle(getString(R.string.notification_1))
                        .setContentText(getString(R.string.notification_2))
                        .setPriority(Notification.PRIORITY_MAX)
                        .setSmallIcon(R.drawable.path_back)
                        .build();

                notificationManager.notify(notifyId, notification);

                Task task = new Task(getString(R.string.customer_4), getString(R.string.location_2), getString(R.string.type_4));
                movieList.add(task);
                mAdapter.notifyDataSetChanged();

            }
        }, 10000);

    }

    public interface onSetPointListner {
        public void setPoint(String point);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            onSetPointListner = (onSetPointListner) activity;
        } catch (Exception e ) {

        }
    }
}

package com.example.pouria.myapplication;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.graphics.Typeface;

public class G extends Application{
	
	public static Context context;
	public static Activity currentActivity;
	public static Typeface dastnevis;
	public static Typeface iransens_0;
	public static Typeface iransens_light;
	public static Typeface iransens_bold;
	public static Typeface sultanadnan;
	@Override
	public void onCreate() {
		super.onCreate();
		context = getApplicationContext();
		dastnevis = Typeface.createFromAsset(getAssets(), "fonts/DastNevis.otf");
		iransens_0 = Typeface.createFromAsset(getAssets(), "fonts/IRANSANSMOBILE_0.TTF");
		iransens_light = Typeface.createFromAsset(getAssets(), "fonts/IRANSANSMOBILE_LIGHT.TTF");
		iransens_bold = Typeface.createFromAsset(getAssets(), "fonts/SultanAdanBold.ttf");
		sultanadnan = Typeface.createFromAsset(getAssets(), "fonts/SultanAdanBold.ttf");
	}

}

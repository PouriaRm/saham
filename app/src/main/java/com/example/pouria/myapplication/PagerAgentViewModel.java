package com.example.pouria.myapplication;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

/**
 * Created by Pouria on 6/1/2018.
 */

public class PagerAgentViewModel extends ViewModel {
    private MutableLiveData<Integer> messageContainerA;
    private MutableLiveData<Integer> messageContainerB;

    public void init()
    {
        messageContainerA = new MutableLiveData<>();
        messageContainerA.setValue(500);
        messageContainerB = new MutableLiveData<>();
        messageContainerB.setValue(500);
    }

    public void sendMessageToB(int msg)
    {
        messageContainerB.setValue(msg);
    }
    public void sendMessageToA(int msg)
    {
        messageContainerA.setValue(msg);

    }
    public LiveData<Integer> getMessageContainerA() {
        return messageContainerA;
    }

    public LiveData<Integer> getMessageContainerB() {
        return messageContainerB;
    }
}
package com.example.pouria.myapplication;

/**
 * Created by Pouria on 6/15/2018.
 */

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;


public class SplashScreen extends Activity {

    private PrefManager prefManager1;

    // Splash screen timer
    private static int SPLASH_TIME_OUT = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        // Making notification bar transparent
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }

        changeStatusBarColor();
        prefManager1 = new PrefManager(this);
//        Animation imgAnim = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in);;
        ImageView imgSplash = (ImageView) findViewById(R.id.imgLogo);

//        imgSplash.setAnimation(imgAnim);


//        Animation txtAnim = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_up);;
        TextView txtSplash = (TextView) findViewById(R.id.splash_text_view);
        txtSplash.setTypeface(G.sultanadnan);
        TextView txtSplash1 = (TextView) findViewById(R.id.textView);
        txtSplash1.setTypeface(G.dastnevis);

//        txtSplash.setAnimation(txtAnim);

        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity

                Log.i("Splash", " " + prefManager1.isFirstTimeLaunch());
                if (prefManager1.isFirstTimeLaunch()) {
                    Log.i("Splash", " " + prefManager1.isFirstTimeLaunch());
                    prefManager1.setFirstTimeLaunch(false);
                    startActivity(new Intent(SplashScreen.this, WelcomeActivity.class));
                    finish();
                } else {
                    startActivity(new Intent(SplashScreen.this, Main3Activity.class));
                    finish();
                }
//                Intent i = new Intent(SplashScreen.this, MainActivity.class);
//                startActivity(i);
//
//                // close this activity
//                finish();
            }
        }, SPLASH_TIME_OUT);
    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }
}
